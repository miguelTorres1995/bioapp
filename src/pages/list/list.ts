import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { CLONACION, CLONACION_LIST, CULTIVO, CULTIVO_LIST } from '../../assets/constants';
import { ContentPage } from '../content/content';

/**
 * Generated class for the ListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-list',
  templateUrl: 'list.html',
})
export class ListPage {

  tema: string;
  source:string;
  list: any[];

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.tema = '';
    this.list = [];
    this.source = "";
    
    console.log("Params ", this.navParams);
    console.log("Tema ", this.navParams.get('tema'));
    
    console.log("this.list", this.list);

    this.tema = this.navParams.get('tema');
    if (this.tema === CULTIVO) {
      this.list = CULTIVO_LIST;
    } else if (this.tema === CLONACION) {
      this.list = CLONACION_LIST;
    }
    console.log("this.list", this.list);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ListPage');
  }

  goToContent(item){
    console.log(">>> item ", item);       
    this.navCtrl.push(ContentPage,{item:item,tema:this.tema});
  }

  getSource(id){
    return "../../assets/imgs/"+this.tema+"/"+id+".png";
  }
  

}
