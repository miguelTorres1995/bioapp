import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { ListPage } from '../list/list';
import { CLONACION, CULTIVO } from '../../assets/constants';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  CLONACION:string;
  CULTIVO:string;
 
  
  constructor(public navCtrl: NavController) {
    this.CLONACION = CLONACION;
    this.CULTIVO = CULTIVO;
   
  }

  goToList(tema:string){ 
    console.log(">>> en home ", tema);
       
    this.navCtrl.push(ListPage,{tema:tema});
  }

}
