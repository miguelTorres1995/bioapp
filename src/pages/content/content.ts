import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the ContentPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-content',
  templateUrl: 'content.html',
})
export class ContentPage {
  item:any;
  tema:string;
  source:string;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.item = this.navParams.get('item');
    this.tema = this.navParams.get('tema');
    this.source = "../../assets/imgs/"+this.tema+"/"+this.item.id+".png"

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ContentPage');
  }

}
