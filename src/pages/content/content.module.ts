import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ContentPage } from './content';

@NgModule({
  declarations: [
    
  ],
  imports: [
    IonicPageModule.forChild(ContentPage),
  ],
})
export class ContentPageModule {}
