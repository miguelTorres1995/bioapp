
export const CULTIVO: string = 'cultivos';
export const CLONACION: string = 'clonacion';

export const CULTIVO_LIST = [
    {
        'id': 1,
        'title': '¿Qué es?',
        'text': 'Es un método utilizado para cultivar plantas usando disoluciones minerales en vez de suelo agrícola.  Las raíces reciben una solución nutritiva y equilibrada disuelta en agua con algunos de los elementos químicos esenciales para el desarrollo de las plantas, que pueden crecer en una solución mineral únicamente, o bien en un medio inerte, como arena lavada, grava o perlita, entre muchas otras.'
    },
    {
        'id': 2,
        'title': 'Características',
        'text': 'Las plantas absorben los minerales esenciales por medio de iones inorgánicos disueltos en el agua. En condiciones naturales, el suelo actúa como reserva de nutrientes minerales, pero el suelo en sí no es esencial para que la planta crezca. Cuando los nutrientes minerales de la tierra se disuelven en agua, las raíces de la planta son capaces de absorberlos. '
    },
    {
        'id': 3,
        'title': 'Hidroponía y el medio ambiente',
        'text': 'El cultivo sin suelo es justamente un conjunto de técnicas recomendables cuando no hay suelos con aptitudes agrícolas disponibles. El esquema consiste en: una fuente de agua que impulsa por bombeo agua a través del sistema, recipientes con soluciones madre —nutrientes concentrados—, cabezales de riego y canales construidos donde están los sustratos, las plantas, los conductos para aplicación del fertiriego y el recibidor del efluente '
    },
    {
        'id': 4,
        'title': 'Solución nutritiva que se busca',
        'text': 'En cuanto a la solución nutritiva, se busca proveer a la planta de los 13 elementos minerales principales por sus efectos en ella. Estos son:\n'
            + '1.	Nitrógeno\n'
            + '2.	Potasio\n'
            + '3.	Fósforo\n'
            + '4.	Calcio\n'
            + '5.	Magnesio\n'
            + '6.	Azufre\n'
            + '7.	Hierro\n'
            + '8.	Manganeso\n'
            + '9.	Zinc\n'
            + '10.	Boro\n'
            + '11.	Cobre\n'
            + '12.	Silicio\n'
            + '13.	Molibdeno\n'

    },

]

export const CLONACION_LIST = [
    {
        'id': 1,
        'title': '¿Qué es?',
        'text': 'Es​ copia idéntica de un organismo a partir de su ADN) se puede definir como el proceso por el que se consiguen, de forma asexual, 2 ​ copias idénticas de un organismo, célula o molécula ya desarrollado.'
    },
    {
        'id': 2,
        'title': 'Características',
        'text': '•	En primer lugar se necesita clonar las células (producto embrionario), porque no se puede hacer un órgano o parte del "clon" si no se cuenta con las células que forman a dicho cuerpo.\n'
            + '•	Ser parte de un organismo ya "desarrollado", porque la clonación responde a un interés por obtener copias de un determinado organismo, y sólo cuando es adulto se pueden conocer sus características.\n'
            + '•	Por otro lado, se trata de crearlo de forma asexual.2 La reproducción sexual no permite obtener copias idénticas, ya que este tipo de reproducción por su misma naturaleza genera diversidad múltiple.\n'

    },
    {
        'id': 3,
        'title': 'Dato curioso',
        'text': 'El primer clon no natural se hizo en una oveja (Dolly).'
    },
    {
        'id': 4,
        'title': 'Tipos de clonación',
        'text': '* La clonación génica produce copias de genes o segmentos de ADN.\n'
            + ' * La clonación reproductiva produce copias de animales enteros.\n'
            + '* La clonación terapéutica produce células madre embrionarias para experimentos dirigidos a crear tejidos para reemplazar tejidos lesionados o afectados.\n'

    },
    {
        'id': 5,
        'title': '¿Qué animales han sido clonados?',
        'text': 'Además de ganado vacuno y ovejas, otros mamíferos que han sido clonados de células somáticas incluyen:\n'
            + 'Gato\n'
            + 'Venado\n'
            + 'Perro\n'
            + 'Caballo\n'
            + 'Mula\n'
            + 'Buey\n'
            + 'conejo\n'
            + 'Rata\n'
            + 'Un macaco de la India mediante la división de un embrión.\n'
    },
]